package az.ingress.store.controller;

import az.ingress.store.dto.ProductDto;
import az.ingress.store.model.Product;
import az.ingress.store.repository.ProductRepository;
import az.ingress.store.service.ProductServiceImpl;

import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductServiceImpl service;
    private final ProductRepository productRepository;

    @PostMapping
    public ProductDto create(@Valid @RequestBody ProductDto productDto) {
        return service.create(productDto);
    }

    @GetMapping("/{id}")
    public ProductDto getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @PutMapping("/{id}")
    public ProductDto updateById(@Valid @PathVariable Long id,
                                 @RequestBody ProductDto productDto) {
        return service.updateById(id, productDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        service.deleteById(id);
    }

    @GetMapping
    public List<ProductDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/pageable")
    public Page<Product> employeesPageable(Pageable pageable){
        return productRepository.findAll(pageable);
    }

}
