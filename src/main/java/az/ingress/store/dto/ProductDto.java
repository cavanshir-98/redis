package az.ingress.store.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto implements Serializable {

    private static final long serialVersionUID=12343514635151L;


    private Long id;
    private String name;
    private String ingredients;
    private String description;
    private BigDecimal price;
    private String currency;
    private Integer stockCount;
    private UUID imageId;

}
