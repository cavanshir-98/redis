package az.ingress.store.service;

import az.ingress.store.dto.ProductDto;
import az.ingress.store.mapper.ProductMapper;
import az.ingress.store.model.Product;
import az.ingress.store.repository.ProductRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper mapper;

    @Override
    public ProductDto create(ProductDto productDto) {
        return mapper.mapEntityToDto(productRepository.save(mapper.mapDtoToEntity(productDto)));
    }

    @Override
    @Cacheable(cacheNames = "product",key = "#id")
    public ProductDto getById(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Product Id Not Found"));
        return mapper.mapEntityToDto(product);
    }

    @Override
    public ProductDto updateById(Long id, ProductDto productDto) {
        try {
            productRepository.findById(id);

        } catch (RuntimeException runtimeException) {
            runtimeException.printStackTrace();

        }

        productDto.setId(id);
        return mapper.mapEntityToDto(productRepository.save(mapper.mapDtoToEntity(productDto)));
    }


    @Override
    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    @Cacheable(cacheNames = "getAll",key = "#root.method.name")
        public List<ProductDto> getAll() {
        return productRepository.findAll()
                .stream().map(entity -> (entity != null) ? mapper.mapEntityToDto(entity) : null)
                .collect(Collectors.toList());
    }
}
