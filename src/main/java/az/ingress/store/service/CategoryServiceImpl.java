package az.ingress.store.service;

import az.ingress.store.dto.CategoryDto;
import az.ingress.store.mapper.CategoryMapper;
import az.ingress.store.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryMapper mapper;

    @Override
    public CategoryDto create(CategoryDto categoryDto) {
        return mapper.mapEntityToDto(categoryRepository.save(mapper.mapDtoToEntity(categoryDto)));
    }
}
