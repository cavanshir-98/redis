package az.ingress.store.service;

import az.ingress.store.dto.ProductDto;

import java.util.List;
import java.util.Set;

public interface ProductService {
    ProductDto create(ProductDto productDto);

    ProductDto getById(Long id);

    ProductDto updateById(Long id, ProductDto productDto);

    void deleteById(Long id);

    List<ProductDto> getAll();
}
