package az.ingress.store.service;

import az.ingress.store.dto.CategoryDto;

public interface CategoryService {
    CategoryDto create(CategoryDto categoryDto);
}

